<?php
/**
 * @author Karthikeyan
 */
namespace Drupal\microsoft_azure\Common;

class MicrosoftAzureStorageConn{

  /**
	 * Get Microsoft Azure Storage Configuration from database.
	 */
  public function getMicrosoftAzureStorageConfiguration(){
	  return db_select('microsoft_azure','azure')
      ->fields('azure',array('account','primary_key','blob_container'))
      ->condition('azure.name','microsoftazurestorage')
      ->execute()
      ->fetchAssoc();
	}
}

?>