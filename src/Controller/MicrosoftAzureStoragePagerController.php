<?php
/**
 * A class for get a list from DB.
 * 
 * @author Karthikeyan
 */
namespace Drupal\microsoft_azure\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;

class MicrosoftAzureStoragePagerController extends ControllerBase {
	
  public function queryParameters() {
    $header = array(
      'folder' => $this->t('Folder Name'),
      'name' => $this->t('Blob Name(File name)'),
      'url' => $this->t('Image'),
      'operations' => $this->t('Delete'),
  	);
    $query = db_select('microsoft_azure_storage_file', 'azure_file')->extend('Drupal\Core\Database\Query\PagerSelectExtender')->element(0);
    $query->fields('azure_file', array('id', 'container', 'folder', 'name' ,'url'));
    $result = $query
      ->limit(5)
      ->orderBy('azure_file.id')
      ->execute();
    $rows = array();
    while ($row = $result->fetchAssoc()) {
      $rows[] = array(
        'data' => array(
          $row['folder'],
          $row['name'],
          $this->t('<img src="' . $row['url'] . '" width="60" height="60" />'),
          \Drupal::l('Delete', url::fromRoute('microsoft_azure_delete_file', ['id' => $row['id'], 'container' => $row['container'], 'folder' => $row['folder'], 'file_name' => $row['name']])),
        )
      );
    }
    
	  $build['pager_table_azure'] = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t("There are no file stored in Azure storage blob"),
    );
	
    $build['pager_pager_pager'] = array(
      '#type' => 'pager',
      '#element' => 0,
      '#pre_render' => ['Drupal\microsoft_azure\Controller\MicrosoftAzureStoragePagerController::showPagerCacheContext',]
    );
	
    return $build;
  }
	
  /**
   * #pre_render callback for #type => pager that shows the pager cache context.
   */
  public static function showPagerCacheContext(array $pager) {
    return $pager;
  }
}

?>